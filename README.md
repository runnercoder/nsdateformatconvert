# NSDateFormatConvert

[![CI Status](http://img.shields.io/travis/starrykai/NSDateFormatConvert.svg?style=flat)](https://travis-ci.org/starrykai/NSDateFormatConvert)
[![Version](https://img.shields.io/cocoapods/v/NSDateFormatConvert.svg?style=flat)](http://cocoapods.org/pods/NSDateFormatConvert)
[![License](https://img.shields.io/cocoapods/l/NSDateFormatConvert.svg?style=flat)](http://cocoapods.org/pods/NSDateFormatConvert)
[![Platform](https://img.shields.io/cocoapods/p/NSDateFormatConvert.svg?style=flat)](http://cocoapods.org/pods/NSDateFormatConvert)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

NSDateFormatConvert is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "NSDateFormatConvert"
```

## Author

starrykai, starryskykai@gmail.com

## License

NSDateFormatConvert is available under the MIT license. See the LICENSE file for more info.
