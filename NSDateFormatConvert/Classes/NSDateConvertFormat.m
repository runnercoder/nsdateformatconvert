//
//  NSDate+RCRHelp.m
//  jinjin
//
//  Created by 吴恺 on 16/1/7.
//  Copyright © 2016年 wukai. All rights reserved.
//

#import "NSDateConvertFormat.h"

@implementation NSDate (RCRHelp)

+ (NSString *)currentTimeYYYY_MM_DD {
  return [[self class] p_timeWithFormat:@"yyyy-MM-dd"];
}

+ (NSString *)currentTimeYYYY_MM_DD_HH_MM {
  return [[self class] p_timeWithFormat:@"yyyy-MM-dd hh:mm"];
}

+ (NSString *)p_timeWithFormat:(NSString *)format {
  NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
  [dateFormatter setDateFormat:format];
  return [dateFormatter stringFromDate:[NSDate date]];
}

+ (NSString *)timeYYYYMMDDHHMMWithTimeStamp:(NSTimeInterval)timestamp {
  NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
  [dateFormatter setDateFormat:@"yyyy-MM-dd hh:mm"];
  NSDate *date = [NSDate dateWithTimeIntervalSince1970:timestamp];
  return [dateFormatter stringFromDate:date];
}

+ (NSString *)timeDescriptionWithTimeStamp:(NSTimeInterval)timestamp {
  NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
  [dateFormatter setDateFormat:@"yyyy-MM-dd"];
  NSTimeInterval nowTimeStamp = [[NSDate date] timeIntervalSince1970];
  NSTimeInterval commentTimeStamp = timestamp;
  NSDate *date = [NSDate dateWithTimeIntervalSince1970:commentTimeStamp];
  NSTimeInterval offsetTimeStamp = nowTimeStamp - commentTimeStamp;

  NSTimeInterval minute = 60.0;
  NSTimeInterval hour = 60 * 60.0;
  NSTimeInterval day = 24.0 * hour;

  NSTimeInterval offTime = offsetTimeStamp / day;
  if (offTime >= 1.0 && offTime < 30.0) {
    return [NSString stringWithFormat:@"%d天以前", (int)offTime];
  } else if (offTime >= 30.0) {
    return [dateFormatter stringFromDate:date];
  } else if (offTime >= 0 && offTime < 1.0) {
    offTime = offsetTimeStamp / hour;
    if (offTime >= 1) {
      //        24小时内 大于1小时
      return [NSString stringWithFormat:@"%d小时前", (int)offTime];
    } else {
      //        1小时以内
      offTime = offsetTimeStamp / minute;
      if (offTime <= 3) {
        //          三分中以内
        return @"Just Now";
      } else {
        return [NSString stringWithFormat:@"%d分钟以前", (int)offTime];
      }
    }
  } else {
    return @"You are on the Time Machine";
  }
}

+ (NSString *)timeWWMMDDHHZoneOfTimeStamp:(NSTimeInterval)timeStamp {
  NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
  [dateFormatter setDateFormat:@"EEEE,MMMM,dd,h:mm a zz"];
  NSDate *date = [NSDate dateWithTimeIntervalSince1970:timeStamp];

  return [dateFormatter stringFromDate:date];
}

+ (NSString *)timeHmmaOfTimeStamp:(NSTimeInterval)timeStamp {
  NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
  [dateFormatter setDateFormat:@"h:mm a"];
  NSDate *date = [NSDate dateWithTimeIntervalSince1970:timeStamp];

  return [dateFormatter stringFromDate:date];
}
@end
