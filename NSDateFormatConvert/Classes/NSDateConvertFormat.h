//
//  NSDateConvertFormat.h
//  jinjin
//
//  Created by 吴恺 on 16/1/7.
//  Copyright © 2016年 wukai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (RCRHelp)
+ (NSString *)currentTimeYYYY_MM_DD;
+ (NSString *)currentTimeYYYY_MM_DD_HH_MM;
+ (NSString *)timeDescriptionWithTimeStamp:(NSTimeInterval)timestamp;
+ (NSString *)timeYYYYMMDDHHMMWithTimeStamp:(NSTimeInterval)timestamp;
+ (NSString *)timeWWMMDDHHZoneOfTimeStamp:(NSTimeInterval)timeStamp;
+ (NSString *)timeHmmaOfTimeStamp:(NSTimeInterval)timeStamp;
@end
